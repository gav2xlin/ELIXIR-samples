defmodule Solution do
  def print_twice(text) do
    print_text(text)
    print_text(text)
  end

  defp print_text(text), do: IO.puts(text)
end
