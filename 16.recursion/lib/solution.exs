defmodule Solution do
  def range(from, to) when from > to, do: []

  def range(from, to) do
  [from | range(from + 1, to)]
  end

  #def range(from, to) when from <= to do
  #  [from | range(from + 1, to)]
  #end
  #def range(_, _), do: []
end
