defmodule Solution do
  def process(str, num) do
    String.trim(str) |> String.downcase() |> String.duplicate(num)
  end
end
