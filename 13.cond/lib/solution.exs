defmodule Solution do
  def single_win?(a_win, b_win) do
    cond do
    a_win and not b_win -> true
    not a_win and b_win -> true
    true -> false
    end
  end

  def double_win?(a_win, b_win, c_win) do
    cond do
    a_win && b_win && !c_win -> :ab
    a_win && !b_win && c_win -> :ac
    !a_win && b_win && c_win -> :bc
    true -> false
    end
  end
end
