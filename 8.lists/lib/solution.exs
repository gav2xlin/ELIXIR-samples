defmodule Solution do
  def get_second_item(list) do
    hd(list) + hd(tl(list))
  end
end
