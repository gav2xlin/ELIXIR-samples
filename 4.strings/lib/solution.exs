defmodule Solution do
  def concate_and_upcase(str1, str2) do
    String.upcase(String.trim(str1 <> str2), :ascii)
  end
end
